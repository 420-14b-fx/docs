---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "420-14B-FX "
  text: "Programmation orientée objet"
  tagline: 
  actions:
    - theme: brand
      text: Plan de cours
      link: https://gitlab.com/420-14b-fx/contenu/-/blob/main/420-14B-FX%20-%20A24%20-%20MarVez.pdf?ref_type=heads
 #   - theme: alt
 #     text: Équipe Gr. 1
 #     link: https://teams.microsoft.com/l/team/19%3aLhczFhX90BEKspYhfYN8C8HtQwF-BvTnFzgXHTRw5lg1%40thread.tacv2/conversations?#groupId=dddd74ad-d3ee-45cc-9475-bea0eacf28d2&tenantId=c4a847fa-1e2c-4d94-a1ac-2ed81ddb600a
 #   - theme: brand
 #     text: Équipe Gr. 2
 #     link: https://teams.microsoft.com/l/team/19%3ai2f_vZ14ZdxR4LEdnYi97VU3BLR3jeTLlxzuwbhETWM1%40thread.tacv2/conversations?#groupId=f056adc7-1a4c-4ced-90a7-705c561621e0&tenantId=c4a847fa-1e2c-4d94-a1ac-2ed81ddb600a

features:
  - title: Cours en ligne
    link: /base_donnees
    details: 
  - title: Équipe Gr. 1
    icon: 
      src : https://gitlab.com/420-14b-fx/contenu/-/raw/main/msteams.png?ref_type=heads
    link: https://teams.microsoft.com/l/team/19%3ADVQMkrllT4O_eB6S80ap4AMzz1Wbmz_VNTeonPTHjJ81%40thread.tacv2/conversations?groupId=f64cc9c9-a5a2-4dc2-93a0-a0cd508ced20&tenantId=c4a847fa-1e2c-4d94-a1ac-2ed81ddb600a
    details: 
  - title: Équipe Gr. 2
    icon: 
      src : https://gitlab.com/420-14b-fx/contenu/-/raw/main/msteams.png?ref_type=heads
    link: https://teams.microsoft.com/l/team/19%3AU6wtKhmlABD_fWC1SEbfe_iIj_icI3Ni_kDslU90lTo1%40thread.tacv2/conversations?groupId=e4beba33-8827-4c25-8795-31b6865d6e15&tenantId=c4a847fa-1e2c-4d94-a1ac-2ed81ddb600a

    details: 
---

